import os
import shutil
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton, QLineEdit, QTextEdit, QGroupBox, QComboBox
from PyQt5.QtCore import QThread, pyqtSignal

def refresh_firefox_profile(firefox_profiles_path):
    """
    Refreshes the Firefox profile data by copying the contents of the profile_backup folder into the latest release folder.
    """
    # Define the path to the profile_backup folder
    profile_backup_path = os.path.join(firefox_profiles_path, "profile_backup")

    # Find the folder with the greatest release value
    max_release_value = -1
    latest_release_folder = ""

    # Iterate over each folder in the Profiles directory
    for folder in os.listdir(firefox_profiles_path):
        if "default-release-" in folder:
            try:
                release_value = int(folder.split("-")[-1])
                if release_value > max_release_value:
                    max_release_value = release_value
                    latest_release_folder = folder
            except ValueError:
                # Skip if folder name does not end with an integer
                continue

    # Check if the latest release folder was found
    if latest_release_folder:
        latest_release_path = os.path.join(
            firefox_profiles_path, latest_release_folder)

        print(f"Found latest release folder: {latest_release_folder}")

        # Delete the contents of the latest release folder
        for item in os.listdir(latest_release_path):
            item_path = os.path.join(latest_release_path, item)
            if os.path.isfile(item_path):
                os.remove(item_path)
            elif os.path.isdir(item_path):
                shutil.rmtree(item_path)

        # Copy the contents of profile_backup into the latest release folder
        for item in os.listdir(profile_backup_path):
            item_path = os.path.join(profile_backup_path, item)
            if os.path.isfile(item_path):
                shutil.copy(item_path, latest_release_path)
            elif os.path.isdir(item_path):
                shutil.copytree(item_path, os.path.join(
                    latest_release_path, item))

        return True

    else:
        return False


def update_firefox_profile_backup(firefox_profiles_path, release_value):
    """
    Updates the profile_backup folder by copying the contents of the specified release folder into it.
    """
    # Define the path to the profile_backup folder
    profile_backup_path = os.path.join(firefox_profiles_path, "profile_backup")

    target_folder = ""

    # Iterate over each folder in the Profiles directory to find the matching release folder
    for folder in os.listdir(firefox_profiles_path):
        if f"default-release-{release_value}" in folder:
            target_folder = folder
            break

    if target_folder:
        target_path = os.path.join(firefox_profiles_path, target_folder)

        # Delete the contents of the profile_backup folder
        for item in os.listdir(profile_backup_path):
            item_path = os.path.join(profile_backup_path, item)
            if os.path.isfile(item_path):
                os.remove(item_path)
            elif os.path.isdir(item_path):
                shutil.rmtree(item_path)

        # Copy the contents of the specified release folder into profile_backup
        for item in os.listdir(target_path):
            item_path = os.path.join(target_path, item)
            if os.path.isfile(item_path):
                shutil.copy(item_path, profile_backup_path)
            elif os.path.isdir(item_path):
                shutil.copytree(item_path, os.path.join(
                    profile_backup_path, item))

        return True

    else:
        return False


class RefreshFirefoxProfileThread(QThread):
    output = pyqtSignal(str)

    def __init__(self, firefox_profiles_path):
        super().__init__()
        self.firefox_profiles_path = firefox_profiles_path

    def run(self):
        self.output.emit("Refreshing Firefox profile data...")

        if (refresh_firefox_profile(self.firefox_profiles_path)):
            self.output.emit("Successfully refreshed Firefox profile data.")
        else:
            self.output.emit("Could not find a Firefox profile to refresh.")


class UpdateBackupThread(QThread):
    output = pyqtSignal(str)

    def __init__(self, release_value, firefox_profiles_path):
        super().__init__()
        self.release_value = release_value
        self.firefox_profiles_path = firefox_profiles_path

    def run(self):
        self.output.emit(
            f"Updating profile_backup from default-release-{self.release_value}...")

        if (update_firefox_profile_backup(self.firefox_profiles_path, self.release_value)):
            self.output.emit(
                f"Successfully updated profile_backup from default-release-{self.release_value}.")
        else:
            self.output.emit(
                f"Could not find a default-release-{self.release_value} folder to update profile_backup from.")


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.firefox_profiles_path = ""

    def initUI(self):
        layout = QVBoxLayout()

        # File Path Group
        path_group = QGroupBox("Firefox Profile Path")
        path_layout = QVBoxLayout()
        self.path_text = QLineEdit()
        self.path_text.setPlaceholderText("Enter Firefox profile path...")
        path_button = QPushButton("Set Path")
        path_button.clicked.connect(self.set_path)
        path_layout.addWidget(self.path_text)
        path_layout.addWidget(path_button)
        path_group.setLayout(path_layout)

        # Refresh Profile Group
        refresh_group = QGroupBox("Refresh Profile")
        refresh_layout = QVBoxLayout()
        refresh_button = QPushButton("Refresh profile")
        refresh_button.clicked.connect(self.refresh_profile)
        refresh_layout.addWidget(refresh_button)
        refresh_group.setLayout(refresh_layout)

        # Update Backup Group
        update_group = QGroupBox("Update Backup")  # This was missing
        update_layout = QVBoxLayout()  # This was missing
        self.update_combo = QComboBox()
        self.update_combo.addItem("Select Release")
        update_button = QPushButton("Update Backup")
        update_button.clicked.connect(self.update_backup)
        update_layout.addWidget(self.update_combo)
        update_layout.addWidget(update_button)
        update_group.setLayout(update_layout) 

        # Feedback Group
        self.feedback_group = QGroupBox("Feedback")
        feedback_layout = QVBoxLayout()
        self.feedback_text = QTextEdit()
        self.feedback_text.setReadOnly(True)
        feedback_layout.addWidget(self.feedback_text)
        self.feedback_group.setLayout(feedback_layout)

        layout.addWidget(path_group)
        layout.addWidget(refresh_group)
        layout.addWidget(update_group)
        layout.addWidget(self.feedback_group)

        self.setLayout(layout)
        self.setWindowTitle('Firefox Profile Manager')
        self.show()

    def set_path(self):
            self.firefox_profiles_path = self.path_text.text()
            self.feedback_text.append(
                f"Set Firefox profile path to: {self.firefox_profiles_path}")
            self.populate_combo()  # Call this method after setting the path

    def refresh_profile(self):
        self.refresh_thread = RefreshFirefoxProfileThread(
            self.firefox_profiles_path)
        self.refresh_thread.output.connect(self.update_feedback)
        self.refresh_thread.start()

    # Inside the update_backup method
    def update_backup(self):
        release_value = self.update_combo.currentText().split("-")[-1]
        if release_value != "Select Release":
            self.update_thread = UpdateBackupThread(
                int(release_value), self.firefox_profiles_path)
            self.update_thread.output.connect(self.update_feedback)
            self.update_thread.start()
        else:
            self.feedback_text.append("Please select a valid release.")

    def update_feedback(self, text):
        self.feedback_text.append(text)

    # Add a new method to populate the QComboBox
    def populate_combo(self):
        self.update_combo.clear()
        self.update_combo.addItem("Select Release")

        release_values = []
        for folder in os.listdir(self.firefox_profiles_path):
            if "default-release-" in folder:
                try:
                    release_value = int(folder.split("-")[-1])
                    release_values.append(release_value)
                except ValueError:
                    continue

        for val in sorted(release_values, reverse=True)[:5]:
            self.update_combo.addItem(f"default-release-{val}")

if __name__ == '__main__':
    app = QApplication([])
    ex = App()
    app.exec_()
