# Firefox Profile Refresher GUI

## Description

It's a python based GUI to backup and update Firefox profile. For some reason I keep having to do this manually every other day, so I decided to automate the process. 
Solves a problem, but not THE problem...

## Usage
Note: I've only tested this on Windows 11. Doesn't have very robust error handling. Use at your own risk...
To use simplyu run the script and follow the prompts. 

```bash
python profile_reset_gui.py
```

The dist folder contains a standalone executable if you just want to click and run.

## Installation
pip install -r requirements.txt
